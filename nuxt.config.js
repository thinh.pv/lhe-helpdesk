// import path from 'path'
// import fs from 'fs'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'LHE - NPP',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/_index.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '~/common/plugins/vue-carousel.js',
      mode: 'client'
    },
    {
      src: '~/common/plugins/video-background.js',
      ssr: false
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: ['~/components', '~/common/components', '~/common/middlewares'],
  },

  // Change directory folder
  dir: {
    layouts: 'common/layouts',
    middleware: 'common/middlewares',
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    // https://www.npmjs.com/package/nuxt-i18n
    ['nuxt-i18n', {
      locales: [
        {
          name: 'VietNam',
          code: 'vie',
          iso: 'vie-VN',
          file: 'vie-VN.js'
        },
        {
          name: 'English',
          code: 'en',
          iso: 'en-US',
          file: 'en-US.js'
        },
      ],
      langDir: 'common/lang/',
      defaultLocale: 'vie',
    }]
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
    icons: {
      /* icon options */
      iconFileName: process.env.NODE_ENV==='staging' ? 'logo_lhe.png': 'icon.png'
    }
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vendor: [
      'vue-apexchart'
    ]
  },

  router: {
    middleware: ['notAuthenticated'],
  },

  rules () {
    const perfLoader = new PerfLoader(this.name, this.buildContext, { resolveModule: this.resolveModule })
    const styleLoader = new StyleLoader(
      this.buildContext,
      { isServer: this.isServer, perfLoader, resolveModule: this.resolveModule }
    )

    const babelLoader = {
      loader: this.resolveModule('babel-loader'),
      options: this.getBabelOptions()
    }

    return [
      {
        test: /\.vue$/i,
        loader: this.resolveModule('vue-loader'),
        options: this.loaders.vue
      },
      {
        test: /\.pug$/i,
        oneOf: [
          {
            resourceQuery: /^\?vue/i,
            use: [{
              loader: this.resolveModule('pug-plain-loader'),
              options: this.loaders.pugPlain
            }]
          },
          {
            use: [
              this.resolveModule('raw-loader'),
              {
                loader: this.resolveModule('pug-plain-loader'),
                options: this.loaders.pugPlain
              }
            ]
          }
        ]
      },
      {
        test: /\.(c|m)?jsx?$/i,
        type: 'javascript/auto',
        exclude: (file) => {
          file = file.split(/node_modules(.*)/)[1]

          // not exclude files outside node_modules
          if (!file) {
            return false
          }

          // item in transpile can be string or regex object
          return !this.modulesToTranspile.some(module => module.test(file))
        },
        use: perfLoader.js().concat(babelLoader)
      },
      {
        test: /\.css$/i,
        oneOf: styleLoader.apply('css')
      },
      {
        test: /\.p(ost)?css$/i,
        oneOf: styleLoader.apply('postcss')
      },
      {
        test: /\.less$/i,
        oneOf: styleLoader.apply('less', {
          loader: this.resolveModule('less-loader'),
          options: this.loaders.less
        })
      },
      {
        test: /\.sass$/i,
        oneOf: styleLoader.apply('sass', {
          loader: this.resolveModule('sass-loader'),
          options: this.loaders.sass
        })
      },
      {
        test: /\.scss$/i,
        oneOf: styleLoader.apply('scss', {
          loader: this.resolveModule('sass-loader'),
          options: this.loaders.scss
        })
      },
      {
        test: /\.styl(us)?$/i,
        oneOf: styleLoader.apply('stylus', {
          loader: this.resolveModule('stylus-loader'),
          options: this.loaders.stylus
        })
      },
      {
        test: /\.(png|jpe?g|gif|svg|webp|avif)$/i,
        use: [{
          loader: this.resolveModule('url-loader'),
          options: Object.assign(
            this.loaders.imgUrl,
            { name: this.getFileName('img') }
          )
        }]
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
        use: [{
          loader: this.resolveModule('url-loader'),
          options: Object.assign(
            this.loaders.fontUrl,
            { name: this.getFileName('font') }
          )
        }]
      },
      {
        test: /\.(apk)$/i,
        use: [{
          loader: this.resolveModule('file-loader'),
          options: Object.assign(
            this.loaders.file,
            { name: this.getFileName('file') }
          )
        }]
      }
    ]
  }
}
