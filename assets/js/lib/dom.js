import { curry, partial, map } from './utils'

const _on = (event, handler, capture, el) => {
  el.addEventListener(event, handler, capture)
  return el
}

const on = curry((event, handler, els) =>
  map(
    partial(_on, event, handler, {}),
    els
  ))

const setStyle = curry((k, v, el) => {
  el.style[k] = v
  return el
})

export {
  on,
  setStyle,
}
