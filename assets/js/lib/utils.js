const curry = (f, ...args) => args.length >= f.length ? f(...args) : curry.bind(this, f, ...args)

const partial = (f, ...args) => f.bind(this, ...args)

const map = curry((f, arr) => Array.isArray(arr) ? arr.map(f) : f(arr))

export {
  curry,
  partial,
  map
}
