export default {
  header: {
    forgotPassword: 'Forgot password',
    timekeeping: 'Timekeeping',
    visit: 'Visit',
    order: 'Order',
    orderDetailsShipment: 'Shipment details',
    orderDetailsRecall: 'Recall Details',
    account: 'Account',
    customer: 'Customer',
    report: 'Report',
    ecommerce: 'Ecommerce',
    socialNetwork: 'Internal social network',
  },
  pages: {
    login: {
      title: 'Login',
      usernameLabel: 'Username',
      usernamePlaceholder: 'Type Username',
      passwordLabel: 'Password',
      passwordPlaceholder: 'Type password',
      remember: 'Remember me',
      forgotPassword: 'Forgot password?',
      submitButton: 'Login',
      errorUsername: 'Invalid username',
      errorRequired: 'You need to fill in this field'
    },
    forgotPassword: {
      progressBar: {
        step1: 'Verify information',
        step2: 'Create new password',
        step3: 'Complete',
      },
      confirmEmail: {
        emailLabel: 'Email',
        emailPlaceholder: 'Type email',
        emailErrorMessage: 'Invalid email',
        requiredErrorMessage: 'You need to fill in this field',
        otpLabel: 'OTP code',
        btnSentOtp: 'Sent OTP',
        btnReSentOtp: 'Re-sent OTP',
        otpErrorMessage: 'Mã OTP không đúng hoặc đã hết hạn',
        submitButton: 'Confirm'
      },
      createNewPassword: {
        newPasswordLabel: 'New password',
        newPasswordPlaceholder: 'Enter your new password',
        requiredErrorMessage: 'You need to fill in this field',
        conFirmNewPasswordLabel: 'Confirm new password',
        conFirmNewPasswordPlaceholder: 'Enter your new password',
        conFirmNewPasswordErrorMessage: 'Password does not match',
        submitButton: 'Create new password'
      },
      success: {
        successMessage: "You have successfully created a password!",
        submitButton: 'Return to Login'
      }
    }
  },
  footer: {}
}