export default {
  header: {
    forgotPassword: 'Quên mật khẩu',
    timekeeping: 'Chấm công',
    visit: 'Ghé thăm',
    order: 'Đơn hàng',
    orderDetailsShipment: 'Chi tiết giao hàng',
    orderDetailsRecall: 'Chi tiết thu hàng',
    account: 'Cá nhân',
    customer: 'Khách hàng',
    report: 'Báo cáo',
    ecommerce: 'Thương mại điện tử',
    socialNetwork: 'Mạng xã hội nội bộ',
  },
  pages: {
    login: {
      title: 'Đăng nhập',
      usernameLabel: 'Tên tài khoản',
      usernamePlaceholder: 'Nhập tên tài khoản',
      passwordLabel: 'Mật khẩu',
      passwordPlaceholder: 'Nhập mật khẩu',
      remember: 'Nhớ mật khẩu',
      forgotPassword: 'Quên mật khẩu?',
      submitButton: 'Đăng nhập',
      errorUsername: 'Tên tài khoản không hợp lệ',
      errorRequired: 'Bạn cần điền vào mục này'
    },
    forgotPassword: {
      progressBar: {
        step1: 'Xác thực thông tin',
        step2: 'Tạo mật khẩu mới',
        step3: 'Hoàn thành',
      },
      confirmEmail: {
        emailLabel: 'Email',
        emailPlaceholder: 'Nhập email',
        emailErrorMessage: 'Email không hợp lệ',
        requiredErrorMessage: 'Bạn cần điền vào mục này',
        otpLabel: 'Mã OTP',
        btnSentOtp: 'Gửi OTP',
        btnReSentOtp: 'Gửi lại OTP',
        otpErrorMessage: 'Mã OTP không đúng hoặc đã hết hạn',
        submitButton: 'Xác nhận'
      },
      createNewPassword: {
        newPasswordLabel: 'Mật khẩu mới',
        newPasswordPlaceholder: 'Nhập mật khẩu mới',
        requiredErrorMessage: 'Bạn cần điền vào mục này',
        conFirmNewPasswordLabel: 'Xác nhận mật khẩu mới',
        conFirmNewPasswordPlaceholder: 'Nhập mật khẩu mới',
        conFirmNewPasswordErrorMessage: 'Mật khẩu không trùng khớp',
        submitButton: 'Tạo mật khẩu mới'
      },
      success: {
        successMessage: "Quý khách đã tạo mật khẩu thành công!",
        submitButton: 'Về trang đăng nhập'
      }
    }
  },
  footer: {}
}