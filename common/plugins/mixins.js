export default {
  data () {
    return {
      windowWidth: 0
    }
  },
  created () {
    this.windowWidth = typeof window !== 'undefined' ? window?.innerWidth : 0
  },
  mounted () {
    window.addEventListener('resize', () => this.windowWidth = window?.innerWidth)
  },
  computed: {
    isMobile () {
      return this.windowWidth < 768
    },
    isDesktop () {
      return this.windowWidth >= 768
    }
  }
}
